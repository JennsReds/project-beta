import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import "./index.css"
import TechnicianForm from './Services/technician-form';
import TechniciansList from './Services/technicians-list';
import AppointmentForm from './Services/appointment-form';
import AppointmentsList from './Services/appointment-list';
import AppointmentHistory from './Services/appointment-history'
import ManufacturerForm from './Inventory/manufacturer-form';
import AutomobileForm from './Inventory/automobile-form';
import VehicleModelForm from './Inventory/vehicle-model-form';
import SalespersonForm from './Sales/SalespersonForm';
import ListSalespeople from './Sales/ListSalespeople';
import ListCustomers from './Sales/ListCustomers';
import ListSales from './Sales/ListSales';
import CustomerForm from './Sales/CustomerForm';
import ListManufactuerers from './Inventory/ListManufactuerers';
import ListVehicles from './Inventory/ListVehicles';
import SalesPersonHistory from './Sales/SaleHistory';
import ListAutos from './Inventory/ListAuto';
import SaleForm from './Sales/SaleForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='sales/'>
            <Route path="" element={<ListSales />} />
            <Route path="create" element={<SaleForm />} />
          </Route>
          <Route path='salespeople/'>
            <Route path="" element={<ListSalespeople />} />
            <Route path="create" element = {<SalespersonForm />} />
            <Route path="history" element={<SalesPersonHistory />} />
          </Route>
          <Route path='customers/'>
            <Route path="" element={<ListCustomers />} />
            <Route path="create" element={<CustomerForm />} />
          </Route>
          <Route path="technicians" >
            <Route path="" element={<TechniciansList />} />
            <Route path="create" element={<TechnicianForm />} />
          </Route>

          <Route path="appointments" >
            <Route path="" element={<AppointmentsList />} />
            <Route path="create" element={<AppointmentForm />} />
            <Route path="history" element={<AppointmentHistory />} />
          </Route>
          <Route path="manufacturers" >
            <Route path="create" element={<ManufacturerForm />} />
            <Route path="" element = {<ListManufactuerers />} />
          </Route>
          <Route path="models" >
            <Route path="create" element={<VehicleModelForm />} />
            <Route path="" element = {<ListVehicles />} />
          </Route>
          <Route path="automobiles" >
            <Route path="create" element={<AutomobileForm />} />
            <Route path="" element = {<ListAutos />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
