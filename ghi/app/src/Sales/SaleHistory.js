import React, { useEffect, useState } from 'react';

function SalesPersonHistory() {
    const [salespeople, setSalespeople] = useState([]);
    const [sales, setSales] = useState([]);
    const [selectsalesperson, setSalesperson] = useState('');

    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }

    const fetchData = async () => {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        setSalespeople(data.salespeople);
        }
    }
        const fetchSalesData = async () => {
        const salesUrl = `http://localhost:8090/api/sales/`;
        const salesResponse = await fetch(salesUrl);
        if (salesResponse.ok) {
        const salesData = await salesResponse.json();
        setSales(salesData.sales);
        }
    }
    useEffect(() => {
        fetchSalesData();
        fetchData();
    }, []);

    return (
        <div>
        <h1>Salesperson History</h1>
        <select onChange={handleSalespersonChange} value={selectsalesperson} required name="salesperson" id="salesperson">
            <option value="">Choose a salesperson</option>
            {salespeople.map(salesperson => (
            <option key={salesperson.id} value={salesperson.employee_id}>
                {salesperson.first_name} {salesperson.last_name}
            </option>
            ))}
        </select>
        <table className="table table-stripped">
            <thead>
            <tr>
                <th>Salesperson</th>
                <th>Customer</th>
                <th>VIN</th>
                <th>Price</th>
            </tr>
            </thead>
            <tbody>
            {sales.filter(sale => sale.salesperson.employee_id == selectsalesperson).map( sale => {
                    return(
                        <tr key={sale.id} >
                            <td>{sale.salesperson.first_name}</td>
                            <td>{sale.customer.first_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>${sale.price}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
    );
}

export default SalesPersonHistory;
