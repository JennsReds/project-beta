import React, { useEffect, useState } from 'react';

function ListSalespeople(){
    const [salesperson, setSalesperson] = useState([]);

    const fetchData = async() => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);
        if(response.ok){
            const data = await response.json();
            setSalesperson(data.salespeople);
        }else {
            console.error(response);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);


    return(
        <div>
            <h1>Salespeople</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {salesperson?.map(sale => {
                        return(
                            <tr key={sale.id}>
                                <td>{sale.employee_id}</td>
                                <td>{sale.first_name}</td>
                                <td>{sale.last_name}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default ListSalespeople;
