import React, { useEffect, useState } from 'react';

function AppointmentsList(props) {

    const [appointments, setAppointments] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch (url);
        if (response.ok) {
            const data = await response.json();
            const elig_appointments = data.appointments.filter(appointment => (appointment.status.id !== 200 && appointment.status.id !==300));
            setAppointments(elig_appointments)
        }
        else {
            console.error(response);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    const [inventory_vin_list, setInventoryVinList] = useState([]);

    const fetchInventoryData = async () => {
        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch (url);
        if (response.ok) {
            const inventory_data = await response.json();
            let vins = []
            for (const auto of inventory_data.autos) {
                vins.push(auto.vin)
            }
            setInventoryVinList(vins)
        } else {
            console.error(response);
        }
    }
    useEffect(() => {
        fetchInventoryData();
    }, []);

    function handleVIP(vin) {

        let is_vip = "";
        if (inventory_vin_list.includes(vin)) {
            is_vip = "Yes";
        }
        else {
            is_vip = "No";
        }
        return is_vip;
    }

    const handleCancel = async (id) => {

        const response = await fetch('http://localhost:8080/api/appointments/'+id+'/cancel/', {method: "put"});
        if (response.ok) {
            const result = appointments.filter(appointment => appointment.id !== id);
            setAppointments(result);
        }
    }

    const handleFinish = async (id) => {

        const response = await fetch('http://localhost:8080/api/appointments/'+id+'/finish/', {method: "put"});
        if (response.ok) {
            const result = appointments.filter(appointment => appointment.id !== id);
            setAppointments(result);
        }
    }


return (
    <table className="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Date</th>
                <th>Time</th>
                <th>Reason</th>
                <th>Status</th>
                <th>VIN</th>
                <th>Is VIP?</th>
                <th>Customer</th>
                <th>Technician</th>
            </tr>
        </thead>
        <tbody>
            {appointments?.map(appointment => {
                return (
                <tr key={appointment.id}>
                    <td>{appointment.id}</td>
                    <td>{(new Date(appointment.date_time)).toLocaleDateString()}</td>
                    <td>{(new Date(appointment.date_time)).toLocaleTimeString()}</td>
                    <td>{appointment.reason}</td>
                    <td>{appointment.status.name}</td>
                    <td>{appointment.vin}</td>
                    <td>{handleVIP(appointment.vin)}</td>
                    <td>{appointment.customer}</td>
                    <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                    <td><button onClick={() => handleFinish(appointment.id)} className="btn btn-primary">Finish</button></td>
                    <td><button onClick={() => handleCancel(appointment.id)} className="btn btn-primary">Cancel</button></td>
                </tr>
                );
            })}
        </tbody>
    </table>
    )
}
export default AppointmentsList;
