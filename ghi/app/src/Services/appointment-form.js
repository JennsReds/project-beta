import React, { useEffect, useState } from 'react';

function AppointmentForm(props) {
    const [formData, setFormData] = useState({
        date: '',
        time: '',
        reason: '',
        vin: '',
        customer: '',
        technician: '',
    })
    const handleFormChange = async (event) => {
        const value = event.target.value;
        const name = event.target.name;
        setFormData( {...formData, [name]: value} );
    }

    const [technicians, setTechnicians] = useState([]);
    const fetchTechnicianData = async () => {
        const url = "http://localhost:8080/api/technicians/";
        const response = await fetch (url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians)
        }
    }
    useEffect(() => {
        fetchTechnicianData();
    }, []);
    const handleSubmit = async (event) => {
        event.preventDefault();

        const appointmentUrl = 'http://localhost:8080/api/appointments/';

        const appt_datetime = new Date(formData.date + " " + formData.time)
        console.log(appt_datetime)
        const appointment_data = {
            date_time: appt_datetime,
            reason: formData.reason,
            vin: formData.vin,
            customer: formData.customer,
            technician: formData.technician
        }

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(appointment_data),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                date: '',
                time: '',
                reason: '',
                vin: '',
                customer: '',
                technician: '',
            })
        }
    }
return(

    <div className="row">
    <div className="offset-3 col-6">
    <div className="shadow p-4 mt-4">
        <h1>Create a new appointment</h1>
        <form onSubmit={handleSubmit} id="create-appointment-form">
        {/* <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Date Time" required type="date_time" name="date_time" id="date_time" value={formData.date_time} className="form-control"/>
            <label htmlFor="date_time">Date Time</label>
        </div> */}
        <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Date" required type="date" name="date" id="date" value={formData.date} className="form-control"/>
            <label htmlFor="date">Date</label>
        </div>
        <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Time" required type="time" name="time" id="time" value={formData.time} className="form-control"/>
            <label htmlFor="date_time">Time</label>
        </div>
        <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Reason" required type="text" name="reason" id="reason" value={formData.reason} className="form-control"/>
            <label htmlFor="Reason">Reason</label>
        </div>
        <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="VIN" required type="int" name="vin" id="vin" value={formData.vin} className="form-control"/>
            <label htmlFor="VIN">VIN</label>
        </div>
        <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="customer" required type="text" name="customer" id="customer" value={formData.customer} className="form-control"/>
            <label htmlFor="customer">Customer</label>
        </div>
        <div className="mb-3">
            <select onChange={handleFormChange} required name="technician" id="technician" value={formData.technician} className="form-select">
                <option value="">Choose a technician</option>
                {technicians.map((technician) => {
                    return (
                        < option key={technician.id} value={technician.id}>
                            {technician.first_name} {technician.last_name}
                        </option>
                    )
                })}
            </select>
        </div>
        <button className="btn btn-primary">Create</button>
        </form>
    </div>
    </div>
</div>
)
}

export default AppointmentForm;
