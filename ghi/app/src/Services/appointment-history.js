import React, { useEffect, useState } from 'react';

function AppointmentHistory(props) {

    const [appointments, setAppointments] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch (url);
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
        } else {
            console.error(response);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    const [inventory_vin_list, setInventoryVinList] = useState([]);

    const fetchInventoryData = async () => {
        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch (url);
        if (response.ok) {
            const inventory_data = await response.json();
            let vins = []
            for (const auto of inventory_data.autos) {
                vins.push(auto.vin)
            }
            setInventoryVinList(vins)
        } else {
            console.error(response);
        }
    }
    useEffect(() => {
        fetchInventoryData();
    }, []);

    function HandleVIP(vin) {

        let is_vip = "";
        if (inventory_vin_list.includes(vin)) {
            is_vip = "Yes";
        }
        else {
            is_vip = "No";
        }
        return is_vip;
    }

    const [formData, setFormData] = useState({
        vin: '',
    })

    const handleFormChange = async (event) => {
        const value = event.target.value;
        const name = event.target.name;
        setFormData( {...formData, [name]: value} );
    }

    const handleSearch= async (event) => {
        event.preventDefault();
        const elig_appointments = appointments.filter(appointment => (formData.vin === appointment.vin));
        setAppointments(elig_appointments)
    }

return (
    <div>
    <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Search</h1>
            <form onSubmit={handleSearch} id="search-form">
            <div className="form-floating mb-3">
                <input onChange={handleFormChange} placeholder="VIN" required type="text" name="vin" id="vin" value={formData.vin} className="form-control"/>
                <label htmlFor="vin">Search By VIN</label>
            </div>
            <button className="btn btn-primary">Search</button>
            </form>
        </div>
        </div>
    </div>
    <table className="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Date</th>
                <th>Time</th>
                <th>Reason</th>
                <th>Status</th>
                <th>VIN</th>
                <th>Is VIP?</th>
                <th>Customer</th>
                <th>Technician</th>
            </tr>
        </thead>
        <tbody>
            {appointments?.map(appointment => {
                return (
                <tr key={appointment.id}>
                    <td>{appointment.id}</td>
                    <td>{(new Date(appointment.date_time)).toLocaleDateString()}</td>
                    <td>{(new Date(appointment.date_time)).toLocaleTimeString()}</td>
                    <td>{appointment.reason}</td>
                    <td>{appointment.status.name}</td>
                    <td>{appointment.vin}</td>
                    <td>{HandleVIP(appointment.vin)}</td>
                    <td>{appointment.customer}</td>
                    <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                </tr>
                );
            })}
        </tbody>
    </table>
    </div>
    )
}
export default AppointmentHistory;
