import React, { useEffect, useState } from 'react';

function TechniciansList(props) {

    const [technicians, setTechnicians] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8080/api/technicians/";
        const response = await fetch (url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians)
        } else {
            console.error(response);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

return (
    <table className="table table-striped">
        <thead>
            <tr>
                <th>Last Name</th>
                <th>First Name</th>
                <th>Employee ID</th>
            </tr>
        </thead>
        <tbody>
            {technicians?.map(technician => {
                return (
                <tr key={technician.id}>
                    <td>{technician.last_name}</td>
                    <td>{technician.first_name}</td>
                    <td>{technician.employee_id}</td>
                </tr>
                );
            })}
        </tbody>
    </table>
    )
}
export default TechniciansList;
