import React, { useState } from 'react';

function TechnicianForm(props) {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        employee_id: '',
    })
    const handleFormChange = async (event) => {
        const value = event.target.value;
        const name = event.target.name;
        setFormData( {...formData, [name]: value} );
    }
    const handleSubmit = async (event) => {
        event.preventDefault();

        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                first_name: '',
                last_name: '',
                employee_id: '',
            })
        }
    }
return(

    <div className="row">
    <div className="offset-3 col-6">
    <div className="shadow p-4 mt-4">
        <h1>Create a new technician</h1>
        <form onSubmit={handleSubmit} id="create-technician-form">
        <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="First Name" required type="text" name="first_name" id="first_name" value={formData.first_name} className="form-control"/>
            <label htmlFor="first_name">First Name</label>
        </div>
        <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Last Name" required type="text" name="last_name" id="last_name" value={formData.last_name} className="form-control"/>
            <label htmlFor="Last Name">Last Name</label>
        </div>
        <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Employee ID" type="int" name="employee_id" id="employee_id" value={formData.employee_id} className="form-control"/>
            <label htmlFor="employee_id">Employee ID</label>
        </div>
        <button className="btn btn-primary">Create</button>
        </form>
    </div>
    </div>
</div>
)
}

export default TechnicianForm;
