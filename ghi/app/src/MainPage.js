import icon from './img/icon.webp'
import "./index.css"

function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold" id="Dreamyfont">Dream Wheels</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management!
          <img src = {icon} alt="LOGO"className='carlogo'></img>
        </p>
      </div>
    </div>
  );
}

export default MainPage;
