import React, { useEffect, useState } from 'react';

function ManufacturerList(props) {

    const [manufacturers, setManufacturers] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch (url);
        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setManufacturers(data.manufacturers)
        } else {
            console.error(response);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

return (
    <table className="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
            </tr>
        </thead>
        <tbody>
            {manufacturers?.map(manufacturer => {
                return (
                <tr key={manufacturer.id}>
                    <td>{manufacturer.id}</td>
                    <td>{manufacturer.name}</td>
                </tr>
                );
            })}
        </tbody>
    </table>
    )
}
export default ManufacturerList;
