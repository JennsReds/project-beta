import React, { useEffect, useState } from 'react';

function ListAutos(){
    const [automobiles, setAutomobiles] = useState([]);

    async function fetchData() {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos)
        }
        }
        useEffect(() => {fetchData();}, []);

    return(
        <div>
            <div>
                <h1>Automobiles</h1>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                    <th>Vin</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                    <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {automobiles?.map(auto => {
                        return(
                            <tr key={auto.id}>
                                <td>{auto.vin}</td>
                                <td>{auto.color}</td>
                                <td>{auto.year}</td>
                                <td>{auto.model.name}</td>
                                <td>{auto.model.manufacturer.name}</td>
                                <td>{String(auto.sold)}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}


export default ListAutos;
