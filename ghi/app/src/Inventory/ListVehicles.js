import React, { useEffect, useState } from 'react';

function ListVehicles(){
    const [vehicles, setVehicle] = useState([]);

    const fetchData = async() => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if(response.ok){
            const data = await response.json();
            setVehicle(data.models);
        }else {
            console.error(response);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);


    return(
        <div>
            <h1>Models</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>

                    </tr>
                </thead>
                <tbody>
                    {vehicles?.map(vehicle => {
                        return(
                            <tr key={vehicle.id}>
                                <td>{vehicle.name}</td>
                                <td>{vehicle.manufacturer.name}</td>
                                <td><img src = {vehicle.picture_url} height="200px" width='300px'></img></td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default ListVehicles;
