import React, { useState } from 'react';

function TechnicianForm(props) {
    const [formData, setFormData] = useState({
        name: '',
    })
    const handleFormChange = async (event) => {
        const value = event.target.value;
        const name = event.target.name;
        setFormData( {...formData, [name]: value} );
    }
    const handleSubmit = async (event) => {
        event.preventDefault();

        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
            const newManufacturer = await response.json();
            setFormData({
                name: '',
            })
        }
    }
return(

    <div className="row">
    <div className="offset-3 col-6">
    <div className="shadow p-4 mt-4">
        <h1>Create a new manufacturer</h1>
        <form onSubmit={handleSubmit} id="create-manufacturer-form">
        <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" value={formData.name} className="form-control"/>
            <label htmlFor="first_name">Name</label>
        </div>
        <button className="btn btn-primary">Create</button>
        </form>
    </div>
    </div>
</div>
)
}

export default TechnicianForm;
