import React, { useEffect, useState } from 'react';

function AutomobileForm(props) {
    const [formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
        model_id: '',
    })
    const handleFormChange = async (event) => {
        const value = event.target.value;
        const name = event.target.name;
        setFormData( {...formData, [name]: value} );
    }
    const [models, setModels] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8100/api/models/";
        const response = await fetch (url);
        if (response.ok) {
            const data = await response.json();
            setModels(data.models)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    const handleSubmit = async (event) => {
        event.preventDefault();

        const hatUrl = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newAutomobile = await response.json();
            setFormData({
                color: '',
                year: '',
                vin: '',
                model_id: '',
            })
        }
    }
return(

    <div className="row">
    <div className="offset-3 col-6">
    <div className="shadow p-4 mt-4">
        <h1>Create a New Automobile</h1>
        <form onSubmit={handleSubmit} id="create-automobile-form">
        <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Color" required type="text" name="color" id="color" value={formData.color} className="form-control"/>
            <label htmlFor="color">Color</label>
        </div>
        <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Year" required type="text" name="year" id="year" value={formData.year} className="form-control"/>
            <label htmlFor="Year">Year</label>
        </div>
        <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="VIN" type="text" name="vin" id="vin" value={formData.vin} className="form-control"/>
            <label htmlFor="vin">VIN</label>
        </div>
        <div className="mb-3">
            <select onChange={handleFormChange} required name="model_id" id="model_id" value={formData.model_id} className="form-select">
                <option value="">Choose a model</option>
                {models?.map((model) => {
                    return (
                        < option key={model.id} value={model.id}>
                            {model.name}
                        </option>
                    )
                })}
            </select>
        </div>
        <button className="btn btn-primary">Create</button>
        </form>
    </div>
    </div>
</div>
)
}

export default AutomobileForm;
