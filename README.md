# CarCar

Team:

* Jennifer Rojas - Sales
* Kristen Lynch, The Mechanic - Service

## Design

Trouble managing all your different car dealerships? CarCar is here for you.

The application is built on microservices so you can have confidence in the tech and focus on your dealerships.

Inventory: for managing the automobiles you have in inventory.
Supports creating, viewing, updating and deleting:
-Manufacturer: car manufacturers you are selling
-Vehicle Model: all the models you could want
-Automobile: every car in your inventory

Inventory updates upon sale via integration with the sales microservice.

## Service microservice

Service: Manage all your service technicians and appointments.

The application is built on microservices so you can have confidence in the tech and focus on your dealerships.

Supports creating, viewing, updating and deleting:
-Technician: manage your service technicians
-Appointments: manage all your service appointments, including when they are scheduled, the reason, the status, the associated car vin and customer, and the assigned technician
-Appointment Status: manage your appointment statuses

Appointment status updates support automatic 'Create' and API endpoints for 'Finish' and 'Cancel' out of the box. Additional statuses customizable.

Sold vehicles returning for service are automatically flagged as VIPs via integration with the inventory microservice.

Supports the ability to search and filter appointment history by VIN.


## Sales microservice
Sales: Manage all inqueries pertaining to Sales and Customers.

Supports creating, viewing, updating and deleting; A Saleperson(model) which has the properties of employee id and first/last name. A Customer(model) which has the properties of first/last name, address, and phone number.
AutomobileVO(model) with the properites vin and sold that will poll those properties from our inventory. Sale(model) which has the properties of automobile, salesperson, customer and price all connected to their respected departments.

Supports various creation forms for a new Salesperson, Customer, or Sale!
As well as displaying all availible and unsold automobiles, with a Sale History to tract each Sale filtered by the Saleperson who managed the sale.
