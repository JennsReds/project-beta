from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import AutomobileVO, Salesperson, Customer, Sale
from .encoders import SaleEncoder, SalespersonEncoder, CustomerEncoder, AutomobileVOEncoder


@require_http_methods(["GET", "POST"])
def list_salespeople (request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder)
    else:
        content = json.loads(request.body)
        try:
            salespeople = Salesperson.objects.create(**content)
            return JsonResponse(
                salespeople,
                encoder=SalespersonEncoder,
                safe=False)
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "salesperson already exists"})
            response.status_code = 400
            return response
        except:
            response = JsonResponse({"message": "unable to create salesperson"})
            response.status_code = 400
            return response


@require_http_methods(["GET","DELETE"])
def show_salesperson (request, id):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(id=id)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False)
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "salesperson does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(employee_id=id)
            salesperson.delete()
            return JsonResponse( {"confirm": "salesperson Deleted"}, status=200)
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "salesperson does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def list_customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder)
    else:
        content = json.loads(request.body)
        try:
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False)
        except:
            response = JsonResponse({"message": "unable to create customer"})
            response.status_code = 400
            return response

@require_http_methods(["GET", "DELETE"])
def show_customer(request, id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False)
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "customer does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse(
                {"confirm": "Customer Deleted"})
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "customer does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET"])
def list_automobile_vo(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles}, encoder=AutomobileVOEncoder, sold=False)

@require_http_methods(["GET", "POST"])
def list_sales(request, auto_vo_id=None):
    if request.method == "GET":
        try:
            if auto_vo_id is not None:
                sales = Sale.objects.filter(id=auto_vo_id)
            else:
                sales = Sale.objects.all()
            return JsonResponse(
                {"sales": sales},
                encoder = SaleEncoder,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status = 404
            )
    else:
        content = json.loads(request.body)
        try:
            vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "automobile does not exist or has been sold"},
                status=404,
            )

        try:
            salesperson_id = content["salesperson"]
            sale = Salesperson.objects.get(employee_id=salesperson_id)
            content["salesperson"] = sale
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "salesperson does not exist"},
                status=404,
            )

        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "customer does not exist"},
                status=404,
            )
        price = content["price"]
        sales = Sale.objects.create(**content)
        return JsonResponse(
            sales,
            encoder=SaleEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def show_sale(request, id):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=id)
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False)
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "sale record does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=id)
            sale.delete()
            return JsonResponse({"confirm": " Sale deleted"})
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Sale record does not exist"}, status=404)
    else:
        try:
            content = json.loads(request.body)
            Sale.objects.filter(id=id).update(**content)
            salesrecord = Sale.objects.get(id=id)
            return JsonResponse(
                salesrecord,
                encoder=SaleEncoder,
                safe=False)
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Sale record does not exist"})
            response.status_code = 404
            return response
