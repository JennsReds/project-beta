from django.urls import path
from .views import (show_customer,
                    list_customer,
                    show_salesperson,
                    list_salespeople,
                    list_automobile_vo,
                    list_sales,
                    show_sale)

urlpatterns = [
    path("salespeople/", list_salespeople, name="list_salespeople"),
    path("salespeople/<int:id>/", show_salesperson, name="show_salesperson"),
    path("customers/", list_customer, name="list_customer"),
    path("customers/<int:id>/", show_customer, name="show_customer"),
    path("automobiles/", list_automobile_vo, name="list_automobile_vo"),
    path("sales/", list_sales, name="list_sales"),
    path("sales/<int:id>/", show_sale, name="show_sales"),
]
