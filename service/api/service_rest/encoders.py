from common.json import ModelEncoder
from .models import Technician, Appointment, AppointmentStatus


class AppointmentStatusEncoder(ModelEncoder):
    model = AppointmentStatus
    properties = [
        "id",
        "name",
        ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "last_name",
        "first_name",
        "employee_id",
        ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "vin",
        "customer",
        "date_time",
        "technician",
        "reason",
        "status",
        ]
    encoders = {
        "technician": TechnicianEncoder(),
        "status": AppointmentStatusEncoder(),
    }

    # def get_extra_data(self, o):
    #     return {"status": o.status.name}
