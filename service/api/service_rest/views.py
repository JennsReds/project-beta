from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (
    AppointmentStatusEncoder,
    TechnicianEncoder,
    AppointmentEncoder,
)
from .models import Appointment, AppointmentStatus, Technician


@require_http_methods(["GET", "POST"])
def api_list_appointment_statuses(request):
    """
    Lists the appointment statuses.
    """
    if request.method == "GET":
        statuses = AppointmentStatus.objects.all()
        return JsonResponse(
            {"statuses": statuses},
            encoder=AppointmentStatusEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            status = AppointmentStatus.objects.create(**content)
            return JsonResponse(
                status,
                encoder=AppointmentStatusEncoder,
                safe=False,
            )
        except:  # noqa
            response = JsonResponse(
                {"message": "Could not create the appointment status"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    """
    Lists the technicians.
    """
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:  # noqa
            response = JsonResponse(
                {"message": "Could not create the technician"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_technician(request, pk):
    """
    Returns the details for the technician specified
    by the pk parameter.
    """
    if request.method == "GET":
        technician = Technician.objects.get(id=pk)
        return JsonResponse(
            {"hat": technician},
            encoder=TechnicianEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            count, _ = Technician.objects.get(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body)
            Technician.objects.filter(id=pk).update(**content)
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    """
    Lists the appointments.
    """
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)

        # Get the technician and put it in the content dict
        try:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician details"},
                status=400,
            )

        # Get the status and put it in the content dict
        try:
            if "status" in content:
                status = AppointmentStatus.objects.get(id=content["status"])  # noqa
                content["status"] = status
        except AppointmentStatus.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid status"},
                status=400,
            )
        try:
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except:  # noqa
            response = JsonResponse(
                {"message": "Could not create the appointment"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_appointment(request, pk):
    """
    Returns the details for the appointment specified
    by the pk parameter.
    """
    if request.method == "GET":
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            {"appointment": appointment},
            encoder=AppointmentEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            count, _ = Appointment.objects.get(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        content = json.loads(request.body)
        try:
            if "technician" in content:
                technician = Technician.objects.get(id=content["status"])  # noqa
                content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician"},
                status=400,
            )
        try:
            if "status" in content:
                status = AppointmentStatus.objects.get(id=content["status"])  # noqa
                content["status"] = status
        except AppointmentStatus.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid status"},
                status=400,
            )
        try:
            Appointment.objects.filter(id=pk).update(**content)
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    try:
        Appointment.objects.filter(id=pk).update(status=300)
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "Does not exist"})
        response.status_code = 404
        return response


@require_http_methods(["PUT"])
def api_finish_appointment(request, pk):
    try:
        Appointment.objects.filter(id=pk).update(status=200)
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "Does not exist"})
        response.status_code = 404
        return response
