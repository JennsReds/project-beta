from django.contrib import admin
from .models import AppointmentStatus


# Register your models here.
@admin.register(AppointmentStatus)
class AppointmentStatusAdmin(admin.ModelAdmin):
    pass
