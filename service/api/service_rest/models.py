from django.db import models
from django.urls import reverse


# Create your models here.
class Technician(models.Model):
    """
    The Technician model provides the ability to track
    technicians who work on service appointments
    """
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.PositiveSmallIntegerField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


class AutomobileVO(models.Model):
    """
    The Automobile VO model provides the ability to track cars
    and whether they have been sold.
    """
    vin = models.CharField(max_length=25, unique=True)
    sold = models.BooleanField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


class AppointmentStatus(models.Model):
    """
    The Appointment Status model provides a status to a
    Service Appointment.
    """

    id = models.PositiveSmallIntegerField(primary_key=True)
    name = models.CharField(max_length=10, unique=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("id",)  # Default ordering for Status
        verbose_name_plural = "appointment_statuses"  # Fix the pluralization


class Appointment(models.Model):
    """
    The Appointment model provides the ability to track
    service appointments.

    Appointments are associated with service technicians
    """
    date_time = models.DateTimeField()
    reason = models.TextField()
    status = models.ForeignKey(
        AppointmentStatus,
        related_name="appointments",
        on_delete=models.PROTECT,
        null=True,
        default=100,
        blank=True,
    )
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=200)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.PROTECT,
    )
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.pk})

    def __str__(self):
        return f"ID: {self.id} Time: {self.date_time} Status: {self.status}"

    class Meta:
        ordering = ("date_time",)
